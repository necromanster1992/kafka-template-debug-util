# kafka-template-debug-util

### Info

It is a kafka util, which will help you to debug your applications.\
To use it completely you should:

- Add **Server**
- Create **Topic**
- Make **Message** template
- Make request **Template**
- **Publish** your request
- Download the log file

### Setup (Jar)

1. Download
   the [JAR file](https://gitlab.com/necromanster1992/kafka-template-debug-util/-/raw/develop/src/setup/kafka-template-debug-util-v1.0.0.jar?ref_type=heads&inline=false)

2. Run the application
   ```commandline
    java -jar kafka-template-debug-util-v1.0.0.jar
    ```

### Setup (Maven)

1. Clone the repository
    ```commandline
    git clone https://gitlab.com/necromanster1992/kafka-template-debug-util
    ```

2. Open project directory
    ```commandline
    cd /kafka-template-debug-util
    ```

3. Run the application
    ```commandline
    mvn spring-boot:run
    ```
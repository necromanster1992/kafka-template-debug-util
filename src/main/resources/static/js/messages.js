// noinspection JSUnresolvedReference

let messagesTable = $("#messages-table")

function getMessageId(messageCard) {
    return messageCard.id.substring(8)
}

function getMessageName(messageCard) {
    return messageCard.getElementsByClassName("message-name")[0].textContent
}

function getMessageContent(messageCard) {
    return messageCard.getElementsByClassName("message-content")[0].textContent
}

$("#messages-add-btn").off("click").on("click", function () {
    $("#messages-popup-title").text("New message")
    $("#messages-popup-delete-btn").css("display", "none")
    $("#message-name").val("")
    $("#message-content").val("")
    $("#messages-popup-btn").text("Create").off("click").on("click", function () {
        let name = $("#message-name").val()
        let content = $("#message-content").val()
        if (content.length === 0)
            alert("Content can't be empty")
        else $.ajax({
            url: `/api/messages`,
            type: "POST",
            data: JSON.stringify({
                name: name,
                content: content
            }),
            contentType: "application/json",
            success: function (result) {
                if (!["Content can't be empty"].includes(result)) {
                    let messageId = result
                    $(".no-messages").remove()
                    if (!$("#messages-table").get().length)
                        $(".messages-window").append(`
                            <div id="messages-table" class="row row-cols-3 row-cols-md-3 g-1 scrollbar-white"></div>
                        `)
                    $("#messages-table").append(`
                            <div class="col">
                                <div class="card message-card" id="message-${messageId}">
                                    <div class="card-body message-card-body">
                                        ${name ? '<h6 class="card-title message-name" id="message-' + messageId + '-name">' + name + '</h6>' : ''}
                                        <p class="card-text message-content"
                                           id="message-${messageId}-text">${content}</p>
                                    </div>
                                </div>
                            </div>
                        `)
                    if ($("#messages-list").children().length === 1) {
                        $(`#message-${messageId}`).addClass("active")
                    }
                    bindMessagesActions()
                    $("#messages-popup").modal("hide")
                } else alert(result)
            }
        })
    })
    $("#messages-popup").modal("show")
})

function bindMessagesActions() {
    $("#messages-table > div > .message-card").off("click").on("click", (e) => {
        let messagesTable = $("#messages-table")
        if (!messagesTable.get().length || !messagesTable.find(".no-messages").length) {
            let messageCard = e.target;
            while (!messageCard.classList.contains("message-card"))
                messageCard = messageCard.parentElement
            setPublishMessage(getMessageContent(messageCard))
        }
    }).off("contextmenu").on("contextmenu", function (e) {
        e.preventDefault()
        let messageCard = e.target;
        while (!messageCard.classList.contains("message-card"))
            messageCard = messageCard.parentElement
        $("#messages-popup-title").text("Edit message")
        $("#message-content").val(getMessageContent(messageCard))
        if (getMessageName(messageCard))
            $("#message-name").val(getMessageName(messageCard))
        else $("#message-name").val("")
        let id = getMessageId(messageCard)
        $("#messages-popup-btn").text("Save").off("click").on("click", function () {
            let name = $("#message-name").val()
            let content = $("#message-content").val()
            if (content.length === 0)
                alert("Content can't be empty")
            else $.ajax({
                url: `/api/messages`,
                type: "PUT",
                data: JSON.stringify({
                    id: id,
                    name: name,
                    content: content
                }),
                contentType: "application/json",
                success: function (result) {
                    if (result === "success") {
                        $(".no-messages").remove()
                        let messageCard = $(`#message-${id}`)
                        messageCard.html(`
                                <div class="card-body message-card-body">
                                    ${name ? '<h6 class="card-title message-name" id="message-' + id + '-name">' + name + '</h6>' : ''}
                                    <p class="card-text message-content"
                                       id="message-${id}-text">${content}</p>
                                </div>
                            `)
                        $("#messages-popup").modal("hide")
                    } else alert(result)
                }
            })
        })
        $("#messages-popup-delete-btn").css("display", "block").off("click").on("click", function () {
            let messageCard = e.target;
            while (!messageCard.classList.contains("message-card"))
                messageCard = messageCard.parentElement
            let id = getMessageId(messageCard)
            $.ajax({
                url: `/api/messages?id=${id}`,
                type: "DELETE",
                success: function (result) {
                    if (result === "success") {
                        $(`#message-${id}`).parent().remove()
                        if (messagesTable.children().length === 0) {
                            messagesTable.remove()
                            $(".messages-window").append('<div class="no-messages">No messages</div>')
                        }
                        if (messagesTable.find(".active").length !== 1)
                            messagesTable.find(".message-card:nth-child(1)").addClass("active")
                        $("#messages-popup").modal("hide")
                    } else alert(result)
                }
            })
        })
        $("#messages-popup").modal("show")
    })
}

bindMessagesActions()
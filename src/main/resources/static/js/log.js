function saveLogAsFile(saveButton) {
    let textToWrite = $("#log").val()
    let textFileAsBlob = new Blob([textToWrite], {type: 'text/plain'})
    let server = serverInput.val()
    let topic = topicInput.val()
    saveButton.download = topic + "@" + server + ".log";
    if (window.webkitURL != null) {
        saveButton.href = window.webkitURL.createObjectURL(textFileAsBlob)
    } else {
        saveButton.href = window.URL.createObjectURL(textFileAsBlob)
    }
}

$("#log-save-btn").off("click").on("click", function (e) {
    let downloadButton = e.target;
    while (downloadButton.id !== "log-save-btn")
        downloadButton = downloadButton.parentElement
    saveLogAsFile(downloadButton)
})
let serverInput = $("#server")
let topicInput = $("#topic")
let messageInput = $("#message")
let publishBtn = $("#publish-btn")
let log = $("#log")

function addLog(value) {
    log.val(log.val() + value + "\n")
    log.prop("scrollTop", log.prop("scrollHeight"))
}

function publishButtonLoading() {
    publishBtn.html(`
        <div class="spinner-border text-mediumblue mx-auto" role="status">
          <span class="visually-hidden">Загрузка...</span>
        </div>
    `)
}

function bindPublishButton() {
    publishBtn.on("click", function () {
        publishBtn.addClass("clicked")
        publishButtonLoading()
        publishBtn.off("click")
        $.ajax({
            url: "/publish",
            type: "POST",
            data: JSON.stringify({
                server: serverInput.val(),
                topic: topicInput.val(),
                message: messageInput.val()
            }),
            contentType: "application/json",
            success: function (result) {
                addLog(result)
                bindPublishButton()
                publishBtn.html("Publish")
                publishBtn.removeClass("clicked")
            }
        })
    })
}

messageInput.on("keydown", function (e) {
    if (e.key === "Tab") {
        e.preventDefault();
        let start = this.selectionStart;
        let end = this.selectionEnd;
        this.value = this.value.substring(0, start) + "\t" + this.value.substring(end);
        this.selectionStart =
            this.selectionEnd = start + 1;
    }
})


bindPublishButton()
if (!serversList.find(".no-servers").length) {
    setPublishServer(getActiveServer());
    updateTopics(getActiveServerId())
}

// noinspection JSUnresolvedReference

let templatesTable = $("#templates-table")
let templateServerSelect = $("#template-server")
let templateTopicSelect = $("#template-topic")

function getTemplateMessage(templateCard) {
    return templateCard.getElementsByClassName("template-content")[0].textContent
}

function getTemplateId(templateCard) {
    return templateCard.id.substring(9)
}

function getTemplateName(templateCard) {
    if (templateCard.getElementsByClassName("template-name").length === 0)
        return ""
    return templateCard.getElementsByClassName("template-name")[0].textContent
}

function getTemplateTopic(templateCard) {
    return templateCard.getElementsByClassName("template-server")[0].getElementsByTagName("span")[0].textContent.split("@")[0]
}


function getTemplateServer(templateCard) {
    return templateCard.getElementsByClassName("template-server")[0].getElementsByTagName("span")[0].textContent.split("@")[1]
}

function getTemplateServerId(templateCard) {
    return templateCard.getElementsByClassName("template-server")[0].getElementsByTagName("span")[1].textContent
}

function getTemplateTopicId(templateCard) {
    return templateCard.getElementsByClassName("template-server")[0].getElementsByTagName("span")[2].textContent
}

function getTemplateContent(templateCard) {
    return templateCard.getElementsByClassName("template-content")[0].textContent
}

function fillTemplatePopupServers(templateCard) {
    let serverItems = $("#servers-list").find(".list-group-item")
    if (serverItems[0].classList.contains("no-servers")) {
        templateServerSelect.prop("disabled", true)
        templateServerSelect.html(`<option selected style="color: rgb(170, 170, 170)">No servers</option>`)
    } else {
        templateServerSelect.prop("disabled", false)
        templateServerSelect.html("")
        for (let serverItemId = 0; serverItemId < serverItems.length; serverItemId++) {
            let serverItem = serverItems[serverItemId]
            templateServerSelect.append(`
                <option value="${getServerId(serverItem)}" ${serverItem.classList.contains("active") ? "selected" : ""}>${serverItem.getElementsByTagName("span")[0].textContent}</option>
            `)
        }
        if ($("#templates-popup-title").text() === "Edit template" && templateCard) {
            templateServerSelect.val(getTemplateServerId(templateCard))
        } else templatePopupSelectActiveServer()
    }
}

templateServerSelect.off("change").on("change", () => updateTemplatePopupTopics(null))

function updateTemplatePopupTopics(templateCard) {
    let addressId = templateServerSelect.val()
    if (addressId !== "No servers")
        $.ajax({
            url: `/api/topics?addressId=${addressId}`,
            type: 'GET',
            success: function (result) {
                if (result === 'Server not found')
                    alert('Server not found')
                else fillTemplatePopupTopics(result, templateCard)
            }
        })
    else fillTemplatePopupTopics([], templateCard)
}

function fillTemplatePopupTopics(topics, templateCard) {
    if (templateServerSelect.val() === "No servers" || topics.length === 0) {
        templateTopicSelect.prop("disabled", true)
        templateTopicSelect.html(`<option selected style="color: rgb(170, 170, 170)">No topics</option>`)
    } else {
        templateTopicSelect.prop("disabled", false)
        templateTopicSelect.html("")
        for (let topicId = 0; topicId < topics.length; topicId++) {
            let topic = topics[topicId]
            templateTopicSelect.append(`
                <option value="${topic.id}" ${topicId === 0 ? "selected" : ""}>${topic.name}</option>
            `)
        }
        if ($("#templates-popup-title").text() === "Edit template" && templateCard) {
            templateTopicSelect.val(getTemplateTopicId(templateCard))
        } else templatePopupSelectActiveTopic()
    }
}

function templatePopupSelectActiveServer() {
    templateServerSelect.val(getActiveServerId())
}

function templatePopupSelectActiveTopic() {
    let activeTopic = $("#topics-list > .list-group-item.active")
    let activeTopicId;
    if (activeTopic.get().length) {
        let activeTopicExists = false
        $('#template-topic option').each(function () {
            activeTopicId = getTopicId(activeTopic.get(0))
            if (this.value === activeTopicId) {
                activeTopicExists = true;
                return false;
            }
        })
        if (activeTopicExists) {
            templateTopicSelect.val(activeTopicId)
        }
    }
}

$("#templates-add-btn").off("click").on("click", function () {
    $("#templates-popup-title").text("New template")
    $("#templates-popup-delete-btn").css("display", "none")
    $("#template-name").val("")
    fillTemplatePopupServers()
    updateTemplatePopupTopics()
    $("#template-content").val("")
    $("#templates-popup-btn").text("Create").off("click").on("click", function () {
        let name = $("#template-name").val()
        let topicId = templateTopicSelect.val()
        let topic = templateTopicSelect.find(":selected").text()
        let serverId = templateServerSelect.val()
        let server = templateServerSelect.find(":selected").text()
        let content = $("#template-content").val()
        if (templateServerSelect.val() === "No servers")
            alert("You need to add at least one server to create template")
        else if (templateTopicSelect.val() === "No topics")
            alert("You need to add at least one server to create template")
        else if (content.length === 0)
            alert("Content can't be empty")
        else $.ajax({
                url: `/api/templates`,
                type: "POST",
                data: JSON.stringify({
                    name: name,
                    topicId: topicId,
                    content: content
                }),
                contentType: "application/json",
                success: function (result) {
                    if (!["Content can't be empty"].includes(result)) {
                        let templateId = result
                        $(".no-templates").remove()
                        if (!$("#templates-table").get().length)
                            $(".templates-window").append(`
                                <div id="templates-table" class="row row-cols-3 row-cols-md-3 g-1 scrollbar-white"></div>
                            `)
                        $("#templates-table").append(`
                            <div class="col">
                                <div class="card template-card" id="template-${templateId}">
                                    <div class="card-body template-card-body">
                                        ${name ? '<h6 class="card-title template-name" id="template-' + templateId + '-name">' + name + '</h6>' : ''}
                                        <p class="card-text template-server text-secondary small">
                                            <span>${topic}@${server}</span>
                                            <span class="visually-hidden">${serverId}</span>
                                            <span class="visually-hidden">${topicId}</span>
                                        </p>
                                        <p class="card-text template-content">${content}</p>
                                    </div>
                                </div>
                            </div>
                        `)
                        if ($("#templates-list").children().length === 1) {
                            $(`#template-${templateId}`).addClass("active")
                        }
                        bindTemplatesActions()
                        $("#templates-popup").modal("hide")
                    } else alert(result)
                }
            })
    })
    $("#templates-popup").modal("show")
})

function bindTemplatesActions() {
    $("#templates-table > div > .template-card").off("click").on("click", (e) => {
        let templatesTable = $("#templates-table")
        if (!templatesTable.get().length || !templatesTable.find(".no-templates").length) {
            let templateCard = e.target;
            while (!templateCard.classList.contains("template-card"))
                templateCard = templateCard.parentElement
            setPublishOfTemplate(templateCard)
        }
    }).off("contextmenu").on("contextmenu", function (e) {
        e.preventDefault()
        let templateCard = e.target;
        while (!templateCard.classList.contains("template-card"))
            templateCard = templateCard.parentElement
        $("#templates-popup-title").text("Edit template")
        fillTemplatePopupServers(templateCard)
        updateTemplatePopupTopics(templateCard)
        $("#template-content").val(getTemplateContent(templateCard))
        if (getTemplateName(templateCard))
            $("#template-name").val(getTemplateName(templateCard))
        else $("#template-name").val("")
        let id = getTemplateId(templateCard)
        $("#templates-popup-btn").text("Save").off("click").on("click", function () {
            let name = $("#template-name").val()
            let topicId = templateTopicSelect.val()
            let topic = templateTopicSelect.find(":selected").text()
            let serverId = templateServerSelect.val()
            let server = templateServerSelect.find(":selected").text()
            let content = $("#template-content").val()
            if (templateServerSelect.val() === "No servers")
                alert("You need to add at least one server to create template")
            else if (templateTopicSelect.val() === "No topics")
                alert("You need to add at least one server to create template")
            else if (content.length === 0)
                alert("Content can't be empty")
            else $.ajax({
                    url: `/api/templates`,
                    type: "PUT",
                    data: JSON.stringify({
                        id: id,
                        name: name,
                        topicId: topicId,
                        content: content
                    }),
                    contentType: "application/json",
                    success: function (result) {
                        if (result === "success") {
                            $(".no-templates").remove()
                            let templateCard = $(`#template-${id}`)
                            templateCard.html(`
                                <div class="card-body template-card-body">
                                    ${name ? '<h6 class="card-title template-name" id="template-' + id + '-name">' + name + '</h6>' : ''}
                                    <p class="card-text template-server text-secondary small">
                                        <span>${topic}@${server}</span>
                                        <span class="visually-hidden">${serverId}</span>
                                        <span class="visually-hidden">${topicId}</span>
                                    </p>
                                    <p class="card-text template-content">${content}</p>
                                </div>
                            `)
                            $("#templates-popup").modal("hide")
                        } else alert(result)
                    }
                })
        })
        $("#templates-popup-delete-btn").css("display", "block").off("click").on("click", function () {
            let templateCard = e.target;
            while (!templateCard.classList.contains("template-card"))
                templateCard = templateCard.parentElement
            let id = getTemplateId(templateCard)
            $.ajax({
                url: `/api/templates?id=${id}`,
                type: "DELETE",
                success: function (result) {
                    if (result === "success") {
                        $(`#template-${id}`).parent().remove()
                        if (templatesTable.children().length === 0) {
                            templatesTable.remove()
                            $(".templates-window").append('<div class="no-templates">No templates</div>')
                        }
                        if (templatesTable.find(".active").length !== 1)
                            templatesTable.find(".template-card:nth-child(1)").addClass("active")
                        $("#templates-popup").modal("hide")
                    } else alert(result)
                }
            })
        })
        $("#templates-popup").modal("show")
    })
}

function setPublishServer(server) {
    serverInput.val(server)
}

function setPublishTopic(topic) {
    topicInput.val(topic)
}

function setPublishMessage(message) {
    messageInput.val(message)
}

function setPublishOfTemplate(templateCard) {
    setPublishServer(getTemplateServer(templateCard))
    setPublishTopic(getTemplateTopic(templateCard))
    setPublishMessage(getTemplateMessage(templateCard))
}

bindTemplatesActions()
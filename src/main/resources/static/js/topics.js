// noinspection JSUnresolvedReference

let topicsList = $("#topics-list")
let topicSelect = $("#topic-server")


function getActiveTopicName() {
    return topicsList.find(".active > span:nth-child(1)").text()
}

function getTopicId(topicItem) {
    return topicItem.id.substring(6)
}

function getTopicName(topicItem) {
    return topicItem.getElementsByTagName("span")[0].textContent
}

function getTopicPartitionCount(topicItem) {
    return topicItem.getElementsByTagName("span")[1].textContent
}

function getTopicReplicaCount(topicItem) {
    return topicItem.getElementsByTagName("span")[2].textContent
}

function updateTopics(addressId) {
    topicsListLoading()
    $.ajax({
        url: `/api/topics?addressId=${addressId}`,
        type: 'GET',
        success: function (result) {
            if (result === 'Server not found')
                alert('Server not found')
            else fillTopics(result)
        }
    })
}

function fillTopics(topics) {
    topicsList.html("")
    if (topics.length !== 0) {
        $(".no-topics").remove()
        for (let topicId = 0; topicId < topics.length; topicId++) {
            const topic = topics[topicId]
            // noinspection JSUnresolvedReference
            topicsList.append(`
                <div id="topic-${topic.id}" class="list-group-item list-group-item-action ${topicId === 0 ? 'active' : ''}" data-bs-toggle="list" role="tab">
                    <span>${topic.name}</span>
                    <span class="badge rounded-pill">${topic.partitionCount}</span>
                    <span class="badge rounded-pill">${topic.replicaCount}</span>
                </div>            
            `)
        }
        bindTopicsActions()
        setPublishTopic(getActiveTopicName())
    } else {
        topicsList.append(`<a class="list-group-item list-group-item-action no-topics" data-bs-toggle="list" role="tab">No topics</a>`)
        setPublishTopic("")
    }
}

function fillTopicPopupServers() {
    let serverItems = $("#servers-list").find(".list-group-item")
    if (serverItems[0].classList.contains("no-servers")) {
        topicSelect.prop("disabled", true)
        topicSelect.html(`<option selected style="color: rgb(170, 170, 170)">No servers</option>`)
    } else {
        topicSelect.prop("disabled", false)
        topicSelect.html("")
        for (let serverItemId = 0; serverItemId < serverItems.length; serverItemId++) {
            let serverItem = serverItems[serverItemId]
            topicSelect.append(`
                <option value="${getServerId(serverItem)}" ${serverItem.classList.contains("active") ? "selected" : ""}>${serverItem.getElementsByTagName("span")[0].textContent}</option>
            `)
        }
        topicSelect.val(getActiveServerId())
    }
}

function topicsListLoading() {
    topicsList.html(`
        <div class="spinner-border text-light mx-auto" role="status">
          <span class="visually-hidden">Загрузка...</span>
        </div>
    `)
}

$("#topics-add-btn").off("click").on("click", function () {
    $("#topics-popup-title").text("New topic")
    $("#topics-popup-delete-btn").css("display", "none")
    fillTopicPopupServers()
    $("#topic-name").val("")
    $("#topic-partition-count").val("1")
    $("#topic-replica-count").val("1")
    $("#topics-popup-btn").text("Create").off("click").on("click", function () {
        if (topicSelect.val() === "No servers")
            alert("You need to add at least one server to create topic")
        else {
            let addressId = topicSelect.val()
            let name = $("#topic-name").val()
            let partitionCount = $("#topic-partition-count").val()
            let replicaCount = $("#topic-replica-count").val()
            if (partitionCount === "")
                alert("Partition count must be more than 0")
            else if (partitionCount === "")
                alert("Replica count must be more than 0")
            else $.ajax({
                    url: `/api/topics?addressId=${addressId}&name=${name}&partitionCount=${partitionCount}&replicaCount=${replicaCount}`,
                    type: "POST",
                    success: function (result) {
                        if (![
                            "Topic name can't be blank",
                            "Partition count must be more than 0",
                            "Replica count must be more than 0",
                            "Server not found"
                        ].includes(result)) {
                            let topicId = result
                            $(".no-topics").remove()
                            if (getActiveServerId() === addressId) {
                                topicsList.append(`
                            <div id="topic-${topicId}"
                                 class="list-group-item list-group-item-action"
                                 data-bs-toggle="list" role="tab">
                                <span>${name}</span>
                                <span class="badge rounded-pill">${partitionCount}</span>
                                <span class="badge rounded-pill">${replicaCount}</span>
                            </div>`)
                                if ($("#topics-list").children().length === 1) {
                                    $(`#topic-${topicId}`).addClass("active")
                                }
                            }
                            bindTopicsActions()
                            updateTopics(getActiveServerId())
                            $("#topics-popup").modal("hide")
                        } else alert(result)
                    }
                })
        }
    })
    $("#topics-popup").modal("show")
})

function bindTopicsActions() {
    $("#topics-list > .list-group-item").off("click").on("click", () => {
        if (!topicsList.find(".no-topics").length)
            setPublishTopic(getActiveTopicName())
    }).off("contextmenu").on("contextmenu", function (e) {
        e.preventDefault()
        let topicItem = e.target;
        while (!topicItem.classList.contains("list-group-item"))
            topicItem = topicItem.parentElement
        $("#topics-popup-title").text("Edit topic")
        fillTopicPopupServers()
        $("#topic-name").val(getTopicName(topicItem))
        $("#topic-partition-count").val(getTopicPartitionCount(topicItem))
        $("#topic-replica-count").val(getTopicReplicaCount(topicItem))
        let id = getTopicId(topicItem)
        $("#topics-popup-btn").text("Save").off("click").on("click", function () {
            let addressId = topicSelect.val()
            let name = $("#topic-name").val()
            let partitionCount = $("#topic-partition-count").val()
            let replicaCount = $("#topic-replica-count").val()
            topicsListLoading()
            $.ajax({
                url: `/api/topics?id=${id}&addressId=${addressId}&name=${name}&partitionCount=${partitionCount}&replicaCount=${replicaCount}`,
                type: "PUT",
                success: function (result) {
                    if (result === "success") {
                        $(".no-topics").remove()
                        let topicItem = $(`#topic-${id}`)
                        topicItem.html(`
                            <span>${name}</span>
                            <span class="badge rounded-pill">${partitionCount}</span>
                            <span class="badge rounded-pill">${replicaCount}</span>
                        `)
                        $("#topics-popup").modal("hide")
                        updateTopics(getActiveServerId())
                    } else alert(result)
                }
            })
        })
        $("#topics-popup-delete-btn").css("display", "block").off("click").on("click", function () {
            $.ajax({
                url: `/api/topics?id=${id}`,
                type: "DELETE",
                success: function (result) {
                    if (result === "success") {
                        $(`#topic-${id}`).remove()
                        if (topicsList.children().length === 0) {
                            topicsList.append(`<a class="list-group-item list-group-item-action no-topics" data-bs-toggle="list" role="tab">No topics</a>`)
                        }
                        if (topicsList.find(".active").length !== 1)
                            topicsList.find(".list-group-item:nth-child(1)").addClass("active")
                        $("#topics-popup").modal("hide")
                    } else alert(result)
                }
            })
        })
        $("#topics-popup").modal("show")
    })
}

bindTopicsActions()
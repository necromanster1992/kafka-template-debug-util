let addressInput = $("#address-input")
let topicInput = $("#topic-input")
let messageInput = $("#message-input")
let messages = $("#messages")
let notifications = $("#notifications")


$("#publish-btn").on("click", function () {
    let address = addressInput.val();
    let topic = topicInput.val();
    let message = messageInput.val();
    $.ajax({
        url: `/send?address=${address}&topic=${topic}&message=${message}`,
        type: "POST",
        success: function (result) {
            if (result !== "fail") {
                messages.append(`${result}<br>`)
            }
        }
    })
})
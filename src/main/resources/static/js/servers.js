// noinspection JSUnresolvedReference

let serversList = $("#servers-list")

function getActiveServer() {
    return serversList.find(".active > span:nth-child(1)").text()
}

function getActiveServerId() {
    return serversList.find(".active")[0].id.substring(7)
}

function getServerHost(serverItem) {
    return serverItem.getElementsByTagName("span")[0].textContent.split(":")[0]
}

function getServerPort(serverItem) {
    return serverItem.getElementsByTagName("span")[0].textContent.split(":")[1]
}

function getServerName(serverItem) {
    return serverItem.getElementsByTagName("span")[1].textContent
}

function getServerId(serverItem) {
    return serverItem.id.substring(7)
}

$("#servers-add-btn").off("click").on("click", function () {
    $("#servers-popup-title").text("New server")
    $("#servers-popup-delete-btn").css("display", "none")
    $("#server-host").val("")
    $("#server-port").val("")
    $("#server-name").val("")
    $("#servers-popup-btn").text("Create").off("click").on("click", function () {
        let host = $("#server-host").val()
        let port = $("#server-port").val()
        let name = $("#server-name").val()
        const hostRegExp = new RegExp("(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])")
        const portRegExp = new RegExp("\\d{1,5}")
        if (!host.match(hostRegExp))
            alert("Bad hostname")
        else if (!(port.match(portRegExp) && 0 <= port <= 65535))
            alert("Port must be an integer in 0-65535")
        else $.ajax({
                url: `/api/addresses?host=${host}&port=${port}&name=${name}`,
                type: "POST",
                success: function (result) {
                    if (![
                        "Port must be an integer in 0-65535",
                        "Bad hostname"
                    ].includes(result)) {
                        let serverId = result
                        $(".no-servers").remove()
                        serversList.append(`
                            <div id="server-${serverId}"
                                 class="list-group-item list-group-item-action"
                                 data-bs-toggle="list" role="tab">
                                <span>${host}:${port}</span>
                                ${name ? '<span class="badge rounded-pill">' + name + '</span>' : ''}
                            </div>`)
                        if ($("#servers-list").children().length === 1) {
                            $(`#server-${serverId}`).addClass("active")
                        }
                        bindServersActions()
                        $("#servers-popup").modal("hide")
                    } else alert(result)
                }
            })
    })
    $("#servers-popup").modal("show")
})

function bindServersActions() {
    $("#servers-list > .list-group-item").off("click").on("click", () => {
        if (!serversList.find(".no-servers").length) {
            setPublishServer(getActiveServer())
            updateTopics(getActiveServerId())
        }
    }).off("contextmenu").on("contextmenu", function (e) {
        e.preventDefault()
        let serverItem = e.target;
        while (!serverItem.classList.contains("list-group-item"))
            serverItem = serverItem.parentElement
        $("#servers-popup-title").text("Edit server")
        $("#server-host").val(getServerHost(serverItem))
        $("#server-port").val(getServerPort(serverItem))
        if (getServerName(serverItem))
            $("#server-name").val(getServerName(serverItem))
        else $("#server-name").val("")
        let id = getServerId(serverItem)
        $("#servers-popup-btn").text("Save").off("click").on("click", function () {
            let host = $("#server-host").val()
            let port = $("#server-port").val()
            let name = $("#server-name").val()
            const hostRegExp = new RegExp("(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])")
            const portRegExp = new RegExp("\\d{1,5}")
            if (!host.match(hostRegExp))
                alert("Bad hostname")
            else if (!(port.match(portRegExp) && 0 <= port <= 65535))
                alert("Port must be an integer in 0-65535")
            else $.ajax({
                    url: `/api/addresses?id=${id}&host=${host}&port=${port}&name=${name}`,
                    type: "PUT",
                    success: function (result) {
                        if (result === "success") {
                            $(".no-servers").remove()
                            let serverItem = $(`#server-${id}`)
                            serverItem.html(`
                                <span>${host}:${port}</span>
                                ${name ? '<span class="badge rounded-pill">' + name + '</span>' : ''}
                            `)
                            $("#servers-popup").modal("hide")
                        } else alert(result)
                    }
                })
        })
        $("#servers-popup-delete-btn").css("display", "block").off("click").on("click", function () {
            $.ajax({
                url: `/api/addresses?id=${id}`,
                type: "DELETE",
                success: function (result) {
                    if (result === "success") {
                        $(`#server-${id}`).remove()
                        if (serversList.children().length === 0) {
                            serversList.append(`<a class="list-group-item list-group-item-action no-servers" data-bs-toggle="list" role="tab">No servers</a>`)
                        }
                        if (serversList.find(".active").length !== 1)
                            serversList.find(".list-group-item:nth-child(1)").addClass("active")
                        $("#servers-popup").modal("hide")
                        if (!serversList.children(".no-servers"))
                            updateTopics(getActiveServerId())
                    } else alert(result)
                }
            })
        })
        $("#servers-popup").modal("show")
    })
}

bindServersActions()
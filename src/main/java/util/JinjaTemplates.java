package util;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.hubspot.jinjava.Jinjava;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@SuppressWarnings("UnstableApiUsage")
@Controller
public class JinjaTemplates {
    private static final Jinjava jinjava = new Jinjava();

    JinjaTemplates() {
        jinjava.setResourceLocator((resourceName, charset, jinjavaInterpreter) -> Resources.toString(
                Resources.getResource("templates/" + resourceName),
                StandardCharsets.UTF_8
        ));
    }

    public static String get(@NonNull String templateName) {
        if (!(templateName.endsWith(".jinja")))
            templateName += ".jinja";
        URL path = Resources.getResource("templates/" + templateName);
        String template = null;
        try {
            template = Resources.toString(path, Charsets.UTF_8);
        } catch (IOException e) {
            System.out.println("Can't read template: " + path);
        }
        return template;
    }

    public static String render(String templateName, Map<String, Object> context) {
        return jinjava.render(get(templateName), context);
    }
}
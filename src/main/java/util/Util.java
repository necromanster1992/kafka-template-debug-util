package util;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Util {
    public static void main(String[] args) {
        SpringApplication.run(Util.class, args);
    }
}

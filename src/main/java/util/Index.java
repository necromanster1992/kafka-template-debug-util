package util;

import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.errors.TimeoutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import util.kafka.config.KafkaAdminClient;
import util.kafka.config.KafkaProducerConfig;
import util.kafka.objects.Address;
import util.kafka.objects.Message;
import util.kafka.objects.Template;
import util.kafka.objects.Topic;
import util.kafka.objects.repos.AddressesRepo;
import util.kafka.objects.repos.MessagesRepo;
import util.kafka.objects.repos.TemplatesRepo;
import util.kafka.objects.repos.TopicsRepo;

import java.util.*;

@RestController
@RequestMapping("/")
public class Index {
    private final static String templateName = "index.jinja";
    @Autowired
    private TemplatesRepo templatesRepo;
    @Autowired
    private AddressesRepo addressesRepo;
    @Autowired
    private TopicsRepo topicsRepo;
    @Autowired
    private MessagesRepo messagesRepo;

    @GetMapping
    String index() {
        Iterable<Address> addresses = addressesRepo.findAll();
        Iterable<Message> messages = messagesRepo.findAll();
        Iterable<Template> templates = templatesRepo.findAll();

        Map<String, Object> context = new HashMap<>();
        context.put("templates", templates);
        context.put("addresses", addresses);
        context.put("messages", messages);
        return JinjaTemplates.render(templateName, context);
    }

    @PostMapping("/publish")
    String publish(@RequestBody HashMap<String, String> json) {
        DateFormatter dateFormatter = new DateFormatter("HH:mm:ss dd.MM.yyy");
        String timestamp = "[" + dateFormatter.print(new Date(), Locale.getDefault()) + "]";
        String server = json.get("server");
        if (server.isBlank())
            return timestamp + " Server not specified";
        String topicName = json.get("topic");
        if (topicName.isBlank())
            return timestamp + " Topic not specified";
        String message = Message.getFilledContent(json.get("message"));
        if (message.isBlank())
            return timestamp + " Message not specified";

        KafkaAdminClient client;
        try {
            client = new KafkaAdminClient(server);
        } catch (KafkaException e) {
            return timestamp + " " + "Invalid server: " + server;
        }

        if (client.verifyConnection()) {
            Optional<Topic> topicOptional = topicsRepo.findTopicByName(topicName);
            Topic topic;
            if (topicOptional.isEmpty()) {
                String host = server.split(":")[0];
                int port = Integer.parseInt(server.split(":")[1]);
                Address address = new Address(host, port);
                topic = new Topic(topicName, address);
                address.addTopic(topic);
                addressesRepo.save(address);
                topicsRepo.save(topic);
            } else topic = topicOptional.get();

            try {
                // TODO: 26.08.2023 Implement changing replicas count
                client.createTopic(topic);
            } catch (KafkaException e) {
                return timestamp + " " + e.getLocalizedMessage();
            }

            KafkaTemplate<String, String> kafkaTemplate = new KafkaProducerConfig(server).kafkaTemplate();
            try {
                kafkaTemplate.send(topic.getName(), message);
                kafkaTemplate.flush();
            } catch (TimeoutException e) {
                return timestamp + "\n" + e.getLocalizedMessage();
            }
            if (message.contains("\n"))
                return timestamp + "\n" + topicName + '@' + server + " -> " + message;
            else return timestamp + " " + topicName + '@' + server + " -> " + message;
        } else return timestamp + " " + "Server " + server + " is unavailable";
    }
}

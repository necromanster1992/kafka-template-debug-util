package util.kafka.config;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.ListTopicsOptions;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;
import util.kafka.objects.Topic;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class KafkaAdminClient {
    private final AdminClient client;
    private final static int ADMIN_CLIENT_TIMEOUT_MS = 5000;
    private final String bootstrap;

    public KafkaAdminClient(String bootstrap) {
        this.bootstrap = bootstrap;
        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrap);
        props.put("request.timeout.ms", 3000);
        props.put("connections.max.idle.ms", 5000);
        props.put(AdminClientConfig.RETRIES_CONFIG, 3);

        client = AdminClient.create(props);
    }

    public AdminClient getClient() {
        return client;
    }

    public boolean verifyConnection() {
        try (AdminClient client = getClient()) {
            client.listTopics(new ListTopicsOptions().timeoutMs(ADMIN_CLIENT_TIMEOUT_MS)).listings().get();
        } catch (ExecutionException | InterruptedException e) {
            return false;
        }
        return true;
    }

    @Bean
    public KafkaAdmin admin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put("bootstrap.servers", bootstrap);
        configs.put("request.timeout.ms", 3000);
        configs.put("connections.max.idle.ms", 5000);
        configs.put(AdminClientConfig.RETRIES_CONFIG, 3);
        return new KafkaAdmin(configs);
    }

    public NewTopic createNewTopic(Topic topic) {
        return createNewTopic(topic.getName(), topic.getPartitionCount(), topic.getReplicaCount());
    }

    public NewTopic createNewTopic(String name, int partitionCount, int replicaCount) {
        return TopicBuilder
                .name(name)
                .partitions(partitionCount)
                .replicas(replicaCount)
                .build();
    }

    public void createTopic(Topic topic) {
        admin().createOrModifyTopics(this.createNewTopic(topic));
    }
}

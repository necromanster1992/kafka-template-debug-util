package util.kafka.objects;

import jakarta.persistence.*;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Entity
@Table(name = "messages")
public class Message implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String content;
    private String name;

    public Message() {}

    public Message(String content) {
        this.content = content;
    }

    public Message(String content, String name) {
        this.content = content;
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @SuppressWarnings("unused")
    public String getContent() {
        return content;
    }

    @SuppressWarnings("unused")
    public void setContent(String content) {
        this.content = content;
    }

    public static String getFilledContent(String content) {
        content = insertIdentifier(content);
        content = insertDatetime(content);
        return content;
    }

    private static String insertIdentifier(String s) {
        while (s.contains("{identifier}")) {
            long randomNumber = (long) (Math.random() * Math.pow(10, 11));
            s = s.replace("{identifier}", Long.toString(randomNumber));
        }
        return s;
    }

    private static String insertDatetime(String s) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(timeZone);
        while (s.contains("{datetime}"))
            s = s.replace("{datetime}", df.format(new Date()));
        return s;
    }
}

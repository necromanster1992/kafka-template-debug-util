package util.kafka.objects;

import jakarta.persistence.*;
import util.exceptions.BadPortNumberException;
import util.exceptions.BlankAddressNameException;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String host;
    private int port;
    private String name;
    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Topic> topics = new ArrayList<>();

    public Address() {}

    public Address(String host, int port) {
        setHost(host);
        setPort(port);
    }

    public Address(String host, int port, String name) {
        setHost(host);
        setPort(port);
        setName(name);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @SuppressWarnings("unused")
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @SuppressWarnings("unused")
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if (0 <= port && port <= 65535)
            this.port = port;
        else throw new BadPortNumberException();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.isBlank())
            this.name = name;
        else throw new BlankAddressNameException();
    }

    public void clearName() {
        this.name = null;
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
        topic.setAddress(this);
    }

    public void removeTopic(Topic topic) {
        this.topics.remove(topic);
        topic.setAddress(null);
    }

    public String get() {
        return host + ":" + port;
    }

    @Override
    public String toString() {
        if (name != null)
            return name;
        return host + ":" + port;
    }
}

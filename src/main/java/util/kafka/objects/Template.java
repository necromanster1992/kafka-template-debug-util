package util.kafka.objects;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "templates")
public class Template implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY) private Topic topic;
    private String message;
    private String name;

    public Template() {
    }

    public Template(Topic topic, String message) {
        setTopic(topic);
        setMessage(message);
    }

    public Template(Topic topic, String message, String name) {
        setTopic(topic);
        setMessage(message);
        setName(name);
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

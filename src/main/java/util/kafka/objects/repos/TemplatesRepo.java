package util.kafka.objects.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import util.kafka.objects.Template;

@Repository
public interface TemplatesRepo extends CrudRepository<Template, Long> {
}

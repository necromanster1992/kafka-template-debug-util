package util.kafka.objects.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import util.kafka.objects.Message;

@Repository
public interface MessagesRepo extends CrudRepository<Message, Long> {}

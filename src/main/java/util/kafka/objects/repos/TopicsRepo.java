package util.kafka.objects.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import util.kafka.objects.Address;
import util.kafka.objects.Topic;

import java.util.Optional;

@Repository
public interface TopicsRepo extends CrudRepository<Topic, Long> {
    Iterable<Topic> findAllByAddress(Address address);

    Optional<Topic> findTopicByName(String name);
}

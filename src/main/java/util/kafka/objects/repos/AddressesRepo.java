package util.kafka.objects.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import util.kafka.objects.Address;

@Repository
public interface AddressesRepo extends CrudRepository<Address, Long> {}

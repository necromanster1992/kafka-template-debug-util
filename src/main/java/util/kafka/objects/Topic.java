package util.kafka.objects;

import jakarta.persistence.*;
import org.springframework.lang.NonNull;
import util.exceptions.BlankTopicNameException;

import java.io.Serializable;

@Entity
@Table(name = "topics")
public class Topic implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    private Address address;
    private int partitionCount = 1;
    private int replicaCount = 1;

    public Topic() {}

    public Topic(@NonNull String name,
                 @NonNull Address address) {
        setName(name);
        setAddress(address);
    }

    public Topic(@NonNull String name,
                 @NonNull Address address,
                 @NonNull int partitionCount) {
        setName(name);
        setAddress(address);
        setPartitionCount(partitionCount);
    }

    public Topic(@NonNull String name,
                 @NonNull Address address,
                 @NonNull int partitionCount,
                 @NonNull int replicaCount) {
        setName(name);
        setAddress(address);
        setPartitionCount(partitionCount);
        setReplicaCount(replicaCount);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.isBlank())
            this.name = name;
        else throw new BlankTopicNameException();
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @SuppressWarnings("unused")
    public int getPartitionCount() {
        return partitionCount;
    }

    public void setPartitionCount(int partitionCount) {
        this.partitionCount = partitionCount;
    }

    @SuppressWarnings("unused")
    public int getReplicaCount() {
        return replicaCount;
    }

    public void setReplicaCount(int replicaCount) {
        this.replicaCount = replicaCount;
    }
}

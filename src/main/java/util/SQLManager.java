package util;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;

import javax.sql.DataSource;

@Controller
public class SQLManager {
    private static final JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource());

    SQLManager() {
        createAddressesTable();
        createTopicsTable();
        createMessagesTable();
        createTemplatesTable();
    }

    @Bean
    public static DataSource dataSource() {
        String driverClassName = "org.sqlite.JDBC";
        String dataSourceUrl = "jdbc:sqlite:database.db";

        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(driverClassName);
        dataSourceBuilder.url(dataSourceUrl);
        return dataSourceBuilder.build();
    }

    public void createAddressesTable() {
        jdbcTemplate.execute("""
            CREATE TABLE IF NOT EXISTS addresses(
                id INTEGER NOT NULL PRIMARY KEY,
                host VARCHAR(128) NOT NULL,
                port INTEGER NOT NULL,
                name TEXT(128)
            )
            """);
    }

    public void createTopicsTable() {
        jdbcTemplate.execute("""
            CREATE TABLE IF NOT EXISTS topics(
                id INTEGER NOT NULL PRIMARY KEY,
                name VARCHAR(128) NOT NULL,
                            address_id INTEGER NOT NULL,
                partition_count TEXT(128) NOT NULL,
                            replica_count TEXT(4096) NOT NULL,
                            FOREIGN KEY (address_id) REFERENCES addresses(id)
            )
            """);
    }

    public void createMessagesTable() {
        jdbcTemplate.execute("""
            CREATE TABLE IF NOT EXISTS messages(
                id INTEGER NOT NULL PRIMARY KEY,
                name TEXT(128),
                            content TEXT
            )
            """);
    }

    public void createTemplatesTable() {
        jdbcTemplate.execute("""
                                        CREATE TABLE IF NOT EXISTS templates(
                id INTEGER NOT NULL PRIMARY KEY,
                topic_id INTEGER,
                            message TEXT,
                name TEXT(128),
                            FOREIGN KEY (topic_id) REFERENCES topics(id)
            )
            """);
    }
}
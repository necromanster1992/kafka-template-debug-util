package util.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import util.kafka.objects.Message;
import util.kafka.objects.repos.MessagesRepo;

import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("/api/messages")
public class Messages {
    @Autowired
    private MessagesRepo messagesRepo;

    @GetMapping
    ResponseEntity<Iterable<Message>> get() {
        Iterable<Message> messages = messagesRepo.findAll();
        return ResponseEntity.ok(messages);
    }

    @PostMapping
    String create(@RequestBody HashMap<String, String> json) {
        String content = json.get("content");
        String name = json.get("name");

        if (content.isEmpty())
            return "Content can't be empty";

        Message message;
        if (name.isBlank())
            message = new Message(content);
        else message = new Message(content, name);
        messagesRepo.save(message);
        return message.getId().toString();
    }

    @PutMapping
    String update(@RequestBody HashMap<String, String> json) {
        Long id = Long.valueOf(json.get("id"));
        String content = json.get("content");
        String name = json.get("name");

        Optional<Message> messageOptional = messagesRepo.findById(id);
        if (messageOptional.isEmpty())
            return "Message not found";
        Message message = messageOptional.get();

        if (content.isEmpty())
            return "Message content can't be empty";
        message.setContent(content);

        if (!name.isBlank())
            message.setName(name);

        messagesRepo.save(message);
        return "success";
    }

    @DeleteMapping
    String delete(@RequestParam Long id) {
        Optional<Message> messageOptional = messagesRepo.findById(id);
        if (messageOptional.isEmpty())
            return "Message not found";
        messagesRepo.delete(messageOptional.get());
        return "success";
    }
}

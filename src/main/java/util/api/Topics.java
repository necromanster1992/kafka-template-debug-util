package util.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import util.kafka.objects.Address;
import util.kafka.objects.Topic;
import util.kafka.objects.repos.AddressesRepo;
import util.kafka.objects.repos.TopicsRepo;

import java.util.Optional;

@RestController
@RequestMapping("/api/topics")
public class Topics {
    @Autowired private AddressesRepo addressesRepo;
    @Autowired private TopicsRepo topicsRepo;

    @GetMapping
    ResponseEntity<Iterable<Topic>> get(@RequestParam Long addressId) {
        Optional<Address> addressOptional = addressesRepo.findById(addressId);
        if (addressOptional.isEmpty())
            return ResponseEntity.badRequest().build();
        Address address = addressOptional.get();
        Iterable<Topic> result = topicsRepo.findAllByAddress(address);
        return ResponseEntity.ok(result);
    }

    @PostMapping
    String create(@RequestParam String name,
                  @RequestParam Long addressId,
                  @RequestParam int partitionCount,
                  @RequestParam int replicaCount) {
        if (name.isBlank())
            return "Topic name can't be blank";
        if (partitionCount <= 0)
            return "Partition count must be more than 0";
        if (replicaCount <= 0)
            return "Replica count must be more than 0";

        Optional<Address> addressOptional = addressesRepo.findById(addressId);
        if (addressOptional.isEmpty())
            return "Server not found";
        Address address = addressOptional.get();

        Topic topic = new Topic(name, address, partitionCount, replicaCount);
        topicsRepo.save(topic);
        address.addTopic(topic);
        addressesRepo.save(address);
        return topic.getId().toString();
    }

    @PutMapping
    String update(@RequestParam Long id,
                  @RequestParam String name,
                  @RequestParam Long addressId,
                  @RequestParam int partitionCount,
                  @RequestParam int replicaCount) {
        if (name.isBlank())
            return "Topic name can't be blank";
        if (partitionCount <= 0)
            return "Partition count must be more than 0";
        if (replicaCount <= 0)
            return "Replica count must be more than 0";

        Optional<Topic> topicOptional = topicsRepo.findById(id);
        if (topicOptional.isEmpty())
            return "Topic not found";
        Topic topic = topicOptional.get();

        Optional<Address> addressOptional = addressesRepo.findById(addressId);
        if (addressOptional.isEmpty())
            return "Server not found";
        Address address = addressOptional.get();

        topic.setName(name);
        topic.setPartitionCount(partitionCount);
        topic.setReplicaCount(replicaCount);
        topicsRepo.save(topic);
        address.addTopic(topic);
        addressesRepo.save(address);
        return "success";
    }

    @DeleteMapping
    String delete(@RequestParam Long id) {
        Optional<util.kafka.objects.Topic> topicOptional = topicsRepo.findById(id);
        if (topicOptional.isEmpty())
            return "Topic not found";
        Topic topic = topicOptional.get();
        topic.getAddress().removeTopic(topic);
        topicsRepo.delete(topic);
        return "success";
    }
}

package util.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import util.kafka.objects.Template;
import util.kafka.objects.Topic;
import util.kafka.objects.repos.TemplatesRepo;
import util.kafka.objects.repos.TopicsRepo;

import java.util.HashMap;
import java.util.Optional;

@RestController
@RequestMapping("/api/templates")
public class Templates {
    @Autowired
    private TemplatesRepo templatesRepo;
    @Autowired
    private TopicsRepo topicsRepo;

    @GetMapping
    ResponseEntity<Iterable<util.kafka.objects.Template>> get() {
        Iterable<util.kafka.objects.Template> templates = templatesRepo.findAll();
        return ResponseEntity.ok(templates);
    }

    @PostMapping
    String create(@RequestBody HashMap<String, String> json) {
        Long topicId = Long.valueOf(json.get("topicId"));
        String message = json.get("content");
        String name = json.get("name");
        
        Optional<Topic> topicOptional = topicsRepo.findById(topicId);
        if (topicOptional.isEmpty())
            return "Topic not found";
        Topic topic = topicOptional.get();

        if (message.isEmpty())
            return "Message can't be empty";

        Template template;
        if (name.isBlank())
            template = new util.kafka.objects.Template(topic, message);
        else template = new util.kafka.objects.Template(topic, message, name);
        templatesRepo.save(template);
        return template.getId().toString();
    }

    @PutMapping
    String update(@RequestBody HashMap<String, String> json) {
        Long id = Long.valueOf(json.get("id"));
        Long topicId = Long.valueOf(json.get("topicId"));
        String message = json.get("content");
        String name = json.get("name");

        Optional<util.kafka.objects.Template> templateOptional = templatesRepo.findById(id);
        if (templateOptional.isEmpty())
            return "Template not found";
        Template template = templateOptional.get();

        Optional<Topic> topicOptional = topicsRepo.findById(topicId);
        if (topicOptional.isEmpty())
            return "Topic not found";
        template.setTopic(topicOptional.get());

        if (message.isEmpty())
            return "Message can't be empty";
        template.setMessage(message);

        if (!name.isBlank())
            template.setName(name);

        templatesRepo.save(template);
        return "success";
    }

    @DeleteMapping
    String delete(@RequestParam Long id) {
        Optional<util.kafka.objects.Template> templateOptional = templatesRepo.findById(id);
        if (templateOptional.isEmpty())
            return "Template not found";
        templatesRepo.delete(templateOptional.get());
        return "success";
    }
}

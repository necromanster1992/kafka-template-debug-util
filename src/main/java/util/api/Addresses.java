package util.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import util.kafka.objects.Address;
import util.kafka.objects.repos.AddressesRepo;

import java.util.Optional;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/addresses")
public class Addresses {
    @Autowired private AddressesRepo addressesRepo;

    private String checkAddress(String host, int port) {
        if (0 > port || port > 65535)
            return "Port must be an integer in 0-65535";
        String fullAddress = host + ":" + port;
        String addressPattern = "(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]):\\d{1,5}";
        if (!Pattern.matches(addressPattern, fullAddress))
            return "Bad hostname";
        return "Good address";
    }

    @PostMapping
    String create(@RequestParam String host,
                  @RequestParam int port,
                  @RequestParam String name) {
        String checkAddressResult = checkAddress(host, port);
        if (!checkAddressResult.equals("Good address"))
            return checkAddressResult;

        Address address;
        if (name.isBlank())
            address = new Address(host, port);
        else address = new Address(host, port, name);
        addressesRepo.save(address);
        return address.getId().toString();
    }

    @PutMapping
    String update(@RequestParam Long id,
                  @RequestParam String host,
                  @RequestParam int port,
                  @RequestParam String name) {
        Optional<Address> addressOptional = addressesRepo.findById(id);
        if (addressOptional.isEmpty())
            return "Server not found";
        Address address = addressOptional.get();

        String checkAddressResult = checkAddress(host, port);
        if (!checkAddressResult.equals("Good address"))
            return checkAddressResult;

        address.setHost(host);
        address.setPort(port);
        if (name.isBlank())
            address.clearName();
        else address.setName(name);

        addressesRepo.save(address);
        return "success";
    }

    @DeleteMapping
    String delete(@RequestParam Long id) {
        Optional<util.kafka.objects.Address> addressOptional = addressesRepo.findById(id);
        if (addressOptional.isEmpty())
            return "Server not found";
        addressesRepo.delete(addressOptional.get());
        return "success";
    }
}

package util.exceptions;

public class BlankAddressNameException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Address name can't be blank";
    }
}

package util.exceptions;

public class BlankTopicNameException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Topic name can't be blank";
    }
}

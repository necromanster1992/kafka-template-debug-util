package util.exceptions;

public class BadPortNumberException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Port number must be in 0-65535";
    }
}
